#!/bin/sh
#
# $Id: it8718fd.sh,v 1.2 2009/02/02 03:11:28 trev Exp $
#

# PROVIDE: it8718fd
# REQUIRE: LOGIN
# BEFORE:  securelevel
# KEYWORD: shutdown

# Add the following line to /etc/rc.conf to enable the it8718fd daemon:
#
#	it8718fd_enable="YES"
#
# See it8718fd(8) for it8718f_flags

. /etc/rc.subr

name="it8718fd"
rcvar=`set_rcvar`
command="/usr/local/sbin/${name}"

# Read configuration and set defaults
# DO NOT CHANGE THESE DEFAULTS HERE
# CHANGE THEM IN THE /etc/rc.conf FILE
load_rc_config "$name"
: ${it8718fd_enable="NO"}
: ${it8718fd_flags=""}

run_rc_command "$1"
