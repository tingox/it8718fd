/***************************************************************************
 **
 **  $Id: it8718fd.h,v 1.6 2009/02/05 22:16:33 trev Exp $
 **
 ***************************************************************************
 **
 **  COPYRIGHT NOTICE
 **
 **  Copyright 2009 Trevor B Roydhouse (trev (at) sentry (dot) org)
 **  All rights reserved
 **
 **  Redistribution and use in source and binary forms, with or without
 **  modification, is permitted provided that the following conditions
 **  are met:
 **
 **  1. Redistributions of source code must retain the above copyright
 **     notice, this list of conditions and the following disclaimer.
 **
 **  2. Redistributions in binary form must reproduce the above copyright
 **     notice, this list of conditions and the following disclaimer in the
 **     documentation and/or other materials provided with the distribution.
 **
 **  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND ANY CONTRIBUTORS "AS IS"
 **  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 **  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 **  PURPOSE ARE HEREBY DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR ANY
 **  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 **  EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 **  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 **  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 **  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 **  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 **  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **
 ***************************************************************************
 **              
 **  Name    : it8718fd
 **  File    : it8718fd.h
 **  Date    : 2 January 2009
 **  Author  : Trevor B Roydhouse, BJuris, LLB, LLM (UNSW)
 **
 **  Purpose : Monitor the ITE IT8718F LPC-IO chip's environment controller
 **            for temperatures, fan revolutions and voltages.
 **
 ***************************************************************************/


/* DEFINES */

#define CONFIG_CONTROL_REG	0x02
#define LDN_SELECT_REG		0x07
#define CHIP_ID_BYTE1_REG       0x20
#define CHIP_ID_BYTE2_REG       0x21
#define CHIP_VERSION_REG        0x22
#define SPECIAL_ADDRESS_PORT_1	0x2e
#define SPECIAL_ADDRESS_PORT_2	0x4e
#define EC_BASE_ADDR_MSB	0x60
#define EC_BASE_ADDR_LSB	0x61
#define ISA_PNP_ADDR            0x0279

#define EC_LDN			0x04 /* environment controller */

#define OUTB(x, y) do { u_int tmp = (y); outb(tmp, (x)); } while (0)
#define INB(x) __extension__ ({ u_int tmp = (x); inb(tmp); })
#define CALC_FAN_RPM(val) ((val)==0?-1:(val)==0xffff?0:1350000/(val * 2))

#define FILENAME_LEN		80
#define BUFFER_LEN		512
#define LOCK_FILE		"/var/run/it8718fd.pid"
#define CONFIG_FILE		"/usr/local/etc/it8718fd.conf"
#define PORTNO			11999
#define SECS			5
#define BACKLOG			8 /* pending connection queue number */
#define SYSLOG_LEVEL		LOG_INFO
#define NO_DISPLAY		0
#define VERBOSE_DISPLAY		1
#define COMPACT_DISPLAY		2
#define DEBUG_DISPLAY		3
#define SYSLOG_DISPLAY		4

#define HELP "\nit8718fd v1.13\n\n\
Usage: it8717fd [-c | -d | -v] [-f cfgfile] [-p port] [-s seconds] [-h]\n\n\
  -c | --compact	 Display data in compact foreground mode\n\
  -d | --debug           Display data in debug foreground mode\n\
  -v | --verbose	 Display data in verbose foreground mode\n\
  -f | --config          Config file (default /usr/local/etc/it8718fd.conf)\n\
  -p | --port		 Port no to connect to in daemon mode (default 11999)\n\
  -s | --seconds	 Seconds between data dumps (default 5)\n\
  -h | --help            Show this help\n\n"


/* PROTOTYPES */

void regwrite(uint16_t port, uint8_t reg, uint8_t val);
uint8_t regread(uint16_t port, uint8_t reg);
void enter_config_mode(uint16_t port);
void exit_config_mode(uint16_t port);
int  id_chip(uint16_t port);
void dump_data(uint16_t port, uint8_t ldn_sel, int output_type);
int daemon_reply(char *buffer, int ldn_sel);
void signal_handler(int sig);
void open_lockfile(void);
void write_pid(void);


/* EXTERNAL VARIABLES  (from configfile.c) */

extern char tempin1_label[LABEL_LEN_MAX];
extern char tempin2_label[LABEL_LEN_MAX];
extern char tempin3_label[LABEL_LEN_MAX];
extern char fan1_label[LABEL_LEN_MAX];
extern char fan2_label[LABEL_LEN_MAX];
extern char fan3_label[LABEL_LEN_MAX];
extern char vin0_label[LABEL_LEN_MAX];
extern char vin1_label[LABEL_LEN_MAX];
extern char vin2_label[LABEL_LEN_MAX];
extern char vin3_label[LABEL_LEN_MAX];
extern char vin4_label[LABEL_LEN_MAX];
extern char vin5_label[LABEL_LEN_MAX];
extern char vin6_label[LABEL_LEN_MAX];
extern char vin7_label[LABEL_LEN_MAX];
extern char vbat_label[LABEL_LEN_MAX];


