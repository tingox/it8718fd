/***************************************************************************
 **
 **  $Id: configfile.h,v 1.2 2009/01/05 06:39:17 trev Exp $
 **
 ***************************************************************************
 **
 **  COPYRIGHT NOTICE
 ** 
 **  Copyright 2009 Trevor B Roydhouse (trev (at) sentry (dot) org)
 **  All rights reserved
 **  
 **  Redistribution and use in source and binary forms, with or without
 **  modification, is permitted provided that the following conditions
 **  are met:
 **
 **  1. Redistributions of source code must retain the above copyright
 **     notice, this list of conditions and the following disclaimer.
 **
 **  2. Redistributions in binary form must reproduce the above copyright
 **     notice, this list of conditions and the following disclaimer in the
 **     documentation and/or other materials provided with the distribution.
 **
 **  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND ANY CONTRIBUTORS "AS IS" 
 **  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 **  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 **  PURPOSE ARE HEREBY DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR ANY 
 **  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 **  EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 **  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 **  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY 
 **  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 **  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 **  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **
 ***************************************************************************
 **
 **  Name    : it8718fd
 **  File    : configfile.h
 **  Date    : 3 January 2009
 **  Author  : Trevor B Roydhouse, BJuris, LLB, LLM (UNSW)
 **
 **  Purpose : Handle configuration file reading, parsing.
 **
 ***************************************************************************/


/* Uncomment to compile configfile as a standalone program */
/*
#define STANDALONE
extern int debug = 1;
*/


/* DEFINES */

#define CONF_LINE_MAX		80
#define CONF_PARM_MAX		15
#define LABEL_LEN_MAX		40
#define BUFFER_LEN		512
#define MIN_CONFIG_FILE_SIZE	6
#define MAX_CONFIG_FILE_SIZE	1500

#define TEMPIN1_LABEL		"System"
#define TEMPIN2_LABEL		"CPU"
#define TEMPIN3_LABEL		"Northbridge"

#define FAN1_LABEL		"CPU"
#define FAN2_LABEL		"System"
#define FAN3_LABEL		"Northbridge"

#define VIN0_LABEL		"Vcore"
#define VIN1_LABEL		"VDDR"
#define VIN2_LABEL		"+3.3"
#define VIN3_LABEL		"+5"
#define VIN4_LABEL		"+12"
#define VIN5_LABEL		"-12"
#define VIN6_LABEL		"-5"
#define VIN7_LABEL		"VSB"
#define VBAT_LABEL		"Vbat"


/* PROTOTYPES */

int read_config(char *filename);
void populate_label_defaults(void);
void populate_labels(void);
void dump_labels(void);
int str_index(char string[], char substring[]);


/* EXTERNAL VARIABLES (from it8718fd.c) */

extern int debug;

