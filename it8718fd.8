.\"-
.\"
.\"  $Id: it8718fd.8,v 1.11 2009/02/15 06:28:34 trev Exp $
.\"
.\"  COPYRIGHT NOTICE
.\"
.\"  Copyright 2009 Trevor B Roydhouse (trev (at) sentry (dot) org)
.\"  All rights reserved
.\"
.\"  Redistribution and use in source and binary forms, with or without
.\"  modification, is permitted provided that the following conditions
.\"  are met:
.\"
.\"  1. Redistributions of source code must retain the above copyright
.\"     notice, this list of conditions and the following disclaimer.
.\"
.\"  2. Redistributions in binary form must reproduce the above copyright
.\"     notice, this list of conditions and the following disclaimer in the
.\"     documentation and/or other materials provided with the distribution.
.\"
.\"  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND ANY CONTRIBUTORS "AS IS"
.\"  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
.\"  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
.\"  PURPOSE ARE HEREBY DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR ANY
.\"  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
.\"  EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
.\"  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
.\"  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
.\"  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
.\"  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
.\"  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
.\" manual page [] for it8718fd $Revision: 1.11 $
.Dd January 3, 2009
.Dt IT8718FD 8
.Os FreeBSD
.Sh NAME
.Nm it8718fd
.Nd Motherboard 
.Dq health 
monitor for the ITE Tech. Inc. IT8718F LPC-IO chip.
.Sh SYNOPSIS
.Nm
.Op Fl c | d | v
.Op Fl p Ar port
.Op Fl s Ar seconds
.Op Fl h
.Sh DESCRIPTION
This program reads values from the environment controller of the ITE IT8718F 
LPC-IO chip on a motherboard to enable the monitoring of temperature, fan
revolutions and voltages.
.Pp
.Nm it8718fd
normally runs in the background as a daemon and reports error conditions 
and other information via the 
.Xr syslogd 8
facility. If any of the 
.Dq Fl c d v
flags is specified,
.Nm
will not become a daemon, but will stay in the foreground and 
display the values for all parameters every specified number of
seconds (defaults to 5 if not otherwise specified).
.Pp
Please note that, as its name implies, 
.Nm
only supports the ITE IT8718F chip. The program will exit with an error
message if it does not find this specific chip on your motherboard.
.Sh OPTIONS
The following command line options are available:
.Bl -tag -width Fl
.It Fl c
Displays the environment controller data in compact \f[B]foreground\fP mode.
Does not detach and become a dameon.
.It Fl d
Displays the environment controller data in debug \f[B]foreground\fP mode. This 
produces output with detailed register indices and register values.
Does not detach and become a dameon.
.It Fl v
Displays the environment controller data in verbose \f[B]foreground\fP mode.
Does not detach and become a dameon.
.It Fl f Ar config_file
File name of the configuration file to be used. 
The default is \f[I]/usr/local/etc/it8718fd.conf\f[R].
.It Fl p Ar port
Use a different TCP port number from the default of 11999 for clients 
to connect to the daemon and obtain the current environment controller data.
.It Fl s Ar seconds
The delay between foreground displays of the values from the environment controller
or the delay between syslog data entries if in daemon mode. Set to 0 in daemon mode
to stop syslog data entries.
The default is 5 seconds.
.El
.Sh USAGE
If no command line options are specified, the program will detach and run in dameon
mode listening for client connections on the default TCP port of 11999 and,
if configured,
logging the environment controller data via the syslog facility.
.Pp
.Nm
monitors TCP port 11999 (unless otherwise specified with the \f[B]-p\f[R] option) 
for information requests from clients.
.Ss Examples
.Pp
it8718fd -s 0
.Pp
it8718fd; telnet localhost 11999; killall it8718fd
.Pp
it8718fd -s 300
.Pp
it8718fd -c -s 30
.Pp
it8718fd -d
.El
.Pp
.Sh CONFIGURATION FILE
This file lets you label the environment controller readings for your
particular motherboard. These labels are used in the verbose display mode.
In the future, this file may also specify which environment controller 
readings are active and their acceptable range. 
.Pp
The program does not require a configuration file. If the program does not
find one in the default location and you have not specified an alternative
one on the command line, 
then the program will use its built-in defaults which are:
.Pp
 # Temperature readings
 tempin1 = System      # motherboard dependant
 tempin2 = CPU         # motherboard dependant
 tempin3 = Northbridge # motherboard dependant

 # Fan revolution readings
 fan1 = CPU            # motherboard dependant
 fan2 = System         # motherboard dependant
 fan3 = Northbridge    # motherboard dependant

 # Voltage readings
 vin0 = Vcore
 vin1 = VDDR
 vin2 = +3.3v
 vin3 = +5.0v
 vin4 = +12v
 vin5 = -12v
 vin6 = -5.0v
 vin7 = VSB
 vbat = Vbat

The above is also the correct format for the configuration file.
Leading and trailing spaces are ignored.
All parameters should be in lowercase; values can be in mixed case.
There is a configuration file line length limit of 80 characters and 
the values used for the labels are limited to 40 characters.
Blank lines are ignored and the hash character (#) may be used for
comments.
.El
.Pp
.Sh NOTES
.Ss Supported motherboards
.Bl -bullet -compact
.It 
Gigabyte MA770-DS3 (rev 2.0)
.It
Gigabyte MA790X-DS4 (rev 1.0)
.Pp
.Li Theoretically, any motherboard using an ITE IT8718F LPC-IO chip.
.El
.Pp
.Ss Signals
The following signals have the specified effect when sent to the daemon 
process using the
.Xr kill 1
command:
.Pp
.Bl -tag -width "xxxxx"
.It Dv SIGHUP (hang up)
Causes the daemon to re-read the configuration file
.Pa /usr/local/etc/it8718fd.conf
or the configuration file specified on the commnd line with 
the \f[B]-f\f[R] option.
.It Dv SIGTERM (software termination signal)
Causes the daemon to clean up and exit.
.It Dv SIGKILL (non-catchable, non-ignoreable kill)
Causes the daemon to exit immediately.
.El
.Sh FILES
.Bl -tag -width /usr/local/etc/it8718fd.conf -compact
.It Pa /usr/local/etc/it8718fd.conf
optional configuration file
.It Pa /usr/local/etc/rc.d/it8718fd
startup script
.It Pa /var/run/it8718fd.pid 
the pid of the currently running daemon
.El
.Sh SEE ALSO
.Xr kill 1 ,
.Xr syslog 8
.El
.Sh BUGS
If you come across any "undocumented" features, 
please feel free to report them to me.
.El
.Sh AUTHOR
The
.Nm
utility and this man page were written by 
.An Trevor B Roydhouse 
.Aq trev(at)sentry(dot)org 
for the FreeBSD project, but may be freely used by others.
