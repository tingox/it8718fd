PROGRAM = it8718fd

CC      = gcc
INSTALL = /usr/bin/install
PREFIX  = /usr/local

CFLAGS = -O2 -Wall -Wstrict-prototypes -Wstrict-aliasing \
         -Werror-implicit-function-declaration

OBJS = it8718fd.o configfile.o

all: $(PROGRAM)

it8718fd.o: *.c it8718fd.h

$(PROGRAM): $(OBJS) it8718fd.h
	$(CC) $(CFLAGS) -o $(PROGRAM) $(OBJS)

install: $(PROGRAM)
	$(INSTALL) $(PROGRAM) $(PREFIX)/sbin
	mkdir -p $(PREFIX)/man/man8
	$(INSTALL) $(PROGRAM).8 $(PREFIX)/man/man8
	$(INSTALL) $(PROGRAM).conf.sample $(PREFIX)/etc/
	$(INSTALL) $(PROGRAM).sh $(PREFIX)/etc/rc.d/$(PROGRAM)

clean:
	rm -f $(PROGRAM) *.o

