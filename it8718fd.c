/***************************************************************************
 **
 **  $Id: it8718fd.c,v 1.13 2009/02/05 22:09:11 trev Exp $
 **
 ***************************************************************************
 **
 **  COPYRIGHT NOTICE
 ** 
 **  Copyright 2009 Trevor B Roydhouse (trev (at) sentry (dot) org)
 **  All rights reserved
 **  
 **  Redistribution and use in source and binary forms, with or without
 **  modification, is permitted provided that the following conditions
 **  are met:
 **
 **  1. Redistributions of source code must retain the above copyright
 **     notice, this list of conditions and the following disclaimer.
 **
 **  2. Redistributions in binary form must reproduce the above copyright
 **     notice, this list of conditions and the following disclaimer in the
 **     documentation and/or other materials provided with the distribution.
 **
 **  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND ANY CONTRIBUTORS "AS IS" 
 **  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 **  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 **  PURPOSE ARE HEREBY DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR ANY 
 **  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 **  EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 **  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 **  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY 
 **  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 **  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 **  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **
 ***************************************************************************
 **
 **  Name    : it8718fd
 **  File    : it8718fd.c
 **  Date    : 2 January 2009
 **  Author  : Trevor B Roydhouse, BJuris, LLB, LLM (UNSW)
 **
 **  Purpose : Monitor the ITE IT8718F LPC-IO chip's environment controller
 **            for temperatures, fan revolutions and voltages.
 **
 ***************************************************************************/


/* Includes */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <getopt.h>
#include <sys/types.h>
#include <machine/cpufunc.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <syslog.h>
#include <sys/time.h>
#include "configfile.h"
#include "it8718fd.h"


/* Defines - see it8718fd.h */


/* Prototypes - see it8718fd.h */


/* Globals */

int debug = 0;				/* ye olde debug flag */
int output_type = NO_DISPLAY;		/* output type */
int portno = PORTNO;			/* for socket address */
int seconds = SECS;			/* output delay period */
int lockfile_fd;			/* lock file file descriptor */
uint8_t spec_addr_port = 0;		/* chip special address port */
uint16_t ec_port = 0;			/* environment controller port */
char config_file[FILENAME_LEN] = "";	/* configuration filename */


/************
 **  MAIN  **
 ************/

int main(int argc, char *argv[])
{
	int chip_found = 0;
	int option = 0;			/* getopt */
	int option_index = 0;		/* getopt */
	int io_fd = 0;			/* i/o device file descriptor */
	int ipv4_sock = -1;
	int newsock = 0;		/* socket for each new incoming connection */
	int retval = 0;			/* daemon_reply() return value */
	struct sockaddr_in server;
	socklen_t len = 0;
	char tmpbuffer[BUFFER_LEN] = "";

	static const struct option long_options[] = 
		{
			{"compact",		no_argument, NULL, 'c'},
			{"debug",		no_argument, NULL, 'd'},
			{"timed",		no_argument, NULL, 't'},
			{"verbose",		no_argument, NULL, 'v'},
			{"config",		required_argument, NULL, 'f'},
			{"port",		required_argument, NULL, 'p'},
			{"seconds",		required_argument, NULL, 's'},
			{"help",		no_argument, NULL, 'h'}
		};

	struct itimerval tim = 
		{
 		.it_interval = 
			{
		      	.tv_sec = 0,
			.tv_usec = 0
			},
		.it_value = 
			{
			.tv_sec = 0,
			.tv_usec =  0
			}
		};

	/* Default config filename */
	strncpy(config_file, CONFIG_FILE, FILENAME_LEN);

	/* Process command line options */
	while ((option = getopt_long(argc, argv, "cdf:p:s:vh", long_options, &option_index)) != EOF) 
		{
		switch (option) 
			{
			case 'c':
				/* compact option */
				output_type = COMPACT_DISPLAY;
				break;
			case 'd':
				/* debug option */
				debug = 1;
				output_type = DEBUG_DISPLAY;
				break;
			case 'f':
				/* config file option */
				strncpy(config_file, optarg, FILENAME_LEN);
				break;
			case 'p':
				/* port number option */
				output_type = NO_DISPLAY;
				portno = atoi(optarg);
				break;
			case 's':
				/* seconds option */
				seconds = atoi(optarg);
				tim.it_interval.tv_sec = seconds;
				tim.it_value.tv_sec = seconds;
				break;
			case 'v':
				/* verbose option */
				output_type = VERBOSE_DISPLAY;
				break;
			case 'h':
				/* help option */
				fprintf(stderr, HELP);
				exit(0);
				break;
			default:
				/* unknown option */
				fprintf(stderr, HELP);
				exit(EXIT_FAILURE);
				break;
			}
		}

	/* Populate default parameter values */
	populate_label_defaults();
	
	/* Read configuration file */
	if(read_config(config_file) != 1)
		{
		/* No configuration file */
		if(debug)
			fprintf(stderr, "DEBUG: Using default configuration parameters\n");;
		}
	else
		{
		/* Populate parameter values from configuration file */
		populate_labels();
		}
	
	/* Open the i/o device */
	if ((io_fd = open("/dev/io", O_RDWR)) < 0) 
		{
		perror("Error opening /dev/io");
		fprintf(stderr, "it8718fd must be run as root\n");
		exit(EXIT_FAILURE);
		}

	/* Try first possible config port address */
	chip_found=id_chip(SPECIAL_ADDRESS_PORT_1); 

	/* If chip not found, try alternative config port address */
	if(!chip_found)
		{
		chip_found=id_chip(SPECIAL_ADDRESS_PORT_2);
		}

	/* If no chip found, notify and exit*/
	if(!chip_found)
		{
		fprintf(stderr, "Did not find an ITE IT8718F EC-LPC I/O chip!\n");
		exit(EXIT_FAILURE);
		}

	/* If full daemon mode */
	if(output_type == NO_DISPLAY || output_type == SYSLOG_DISPLAY)
		{
		/* Ensure only one daemon instance can be running */
		open_lockfile();

		/* Daemonise */
		if (daemon(0,0) < 0)
			{
			syslog(LOG_ALERT, "Error calling daemon library: %m");
			syslog(LOG_ALERT, "Aborting");
			exit(EXIT_FAILURE);
			}
		else
			{
			syslog(SYSLOG_LEVEL, "Daemon startup successful");

			/* Setup timer for syslog entries */
			if(seconds > 0)
				{
				/* Init time structure */
				tim.it_interval.tv_sec = seconds;
				tim.it_value.tv_sec = seconds;

				setitimer(ITIMER_REAL, &tim, NULL);
				}

			/* write pid to lock file after daemon call, otherwise get parent pid */
			write_pid();

			/* log it */
			snprintf(tmpbuffer, sizeof(tmpbuffer), "Process id %d", getpid());
			syslog(SYSLOG_LEVEL, tmpbuffer);

			/* Create socket from which to read */
			ipv4_sock = socket(AF_INET, SOCK_STREAM, 0);

			if (ipv4_sock < 0) 
				{
				syslog(LOG_ALERT, "Error opening IPv4 stream socket: %m");
				syslog(LOG_ALERT, "Aborting");
				exit(EXIT_FAILURE);
				}
			else
				{
				syslog(SYSLOG_LEVEL, "IPv4 stream socket opened");
				}

			/* log port number */
			snprintf(tmpbuffer,  sizeof(tmpbuffer), "Local port number %i", portno);
			syslog(SYSLOG_LEVEL, tmpbuffer);

			/* always zero socket address structure before populating and passing to bind() */
			bzero(&server, sizeof(server));

			/* populate socket address structure */
			server.sin_family = AF_INET;
			server.sin_addr.s_addr = htonl(INADDR_ANY);
			server.sin_port = htons(portno);

			/* bind socket to specified port stored in address */
			if (bind(ipv4_sock, (struct sockaddr *)&server, sizeof(struct sockaddr_in)) < 0)
				{
				close(ipv4_sock);
				syslog(LOG_ALERT, "Error binding socket to protocol address: %m");
				syslog(LOG_ALERT, "Aborting");
				exit(EXIT_FAILURE);
				}
			else
				{
				syslog(SYSLOG_LEVEL, "Binding socket to protocol address");
				}

			/* Handle signals */
			signal(SIGCHLD,SIG_IGN); 	/* ignore child */
			signal(SIGTSTP,SIG_IGN); 	/* ignore tty signals */
			signal(SIGTTOU,SIG_IGN);
			signal(SIGTTIN,SIG_IGN);
			signal(SIGHUP,signal_handler); 	/* catch hangup signal */
			signal(SIGTERM,signal_handler); /* catch kill signal */
			signal(SIGALRM,signal_handler); /* catch alarm signal */

			/* Start accepting connections */
			if (ipv4_sock != 0) 
				{
				/* allow queue of BACKLOG pending connections */
				listen(ipv4_sock, BACKLOG);
				}
			
			syslog(SYSLOG_LEVEL, "Waiting for connections ...");

			/* Loop waiting for connections */
			while (1)
				{
				/*
				syslog(SYSLOG_LEVEL, "Waiting for connection ...");
				*/

				/* extract connection request from queue
				   and create new socket for this request */
				newsock = accept(ipv4_sock, NULL, NULL);

				if (newsock == -1)
					{
					syslog(LOG_ALERT, "Error in accept: %m");
					}
				else
					{
					/* Get peer address and port */
					len = sizeof(server);
					getpeername(newsock, (struct sockaddr*)&server, &len);

					/* Log it */
					snprintf(tmpbuffer, sizeof(tmpbuffer), \
						"Connection from %s on port %i", inet_ntoa(server.sin_addr), ntohs(server.sin_port));
					syslog(SYSLOG_LEVEL, tmpbuffer);

					do 
						{
						retval = daemon_reply(tmpbuffer, EC_LDN);

						/* Output to connected socket */
						write(newsock, tmpbuffer, strlen(tmpbuffer));
						} 
					while (retval != 0);
						
					/* 
					syslog(SYSLOG_LEVEL, "Connection terminated");
					*/

					close(newsock);
					}
				}

			}
		}
	/* Otherwise foreground mode */
	else
		{
		while(1)
			{
			dump_data(ec_port, EC_LDN, output_type);
			sleep(seconds);
			}
		}

	close(io_fd);
	return (0);
}


/*********************** 
 **  write_register() **
 ***********************/

void regwrite(uint16_t port, uint8_t reg, uint8_t val)
{
	OUTB(reg, port);
	OUTB(val, port + 1);
}


/*********************** 
 **  read_register()  **
 ***********************/

uint8_t regread(uint16_t port, uint8_t reg)
{
	OUTB(reg, port);
	return INB(port + 1);
}


/***************************
 **  enter_config_mode()  **
 ***************************/

void enter_config_mode(uint16_t port)
{
	OUTB(0x87, port);
	OUTB(0x01, port);
	OUTB(0x55, port);
	/* Special address port 1 sequence finishes with 0x55
	   Alternative special address port 2 sequence finishes with 0xAA */
	OUTB((port == SPECIAL_ADDRESS_PORT_1) ? 0x55 : 0xAA, port);
}


/**************************
 **  exit_config_mode()  **
 **************************/

void exit_config_mode(uint16_t port)
{
	/* Set bit 1 of the configure control register */
	regwrite(port, CONFIG_CONTROL_REG, 0x02);
}


/*****************
 **  id_chip()  **
 *****************/

int id_chip(uint16_t port)
{
	uint16_t id, chipver;
	int retval = 0;

	if(debug)
		fprintf(stderr, "DEBUG: Probing for chip at special address port 0x%02x\n", port);

	enter_config_mode(port);

	id = regread(port, CHIP_ID_BYTE1_REG) << 8;
	id |= regread(port, CHIP_ID_BYTE2_REG);
	chipver = regread(port, CHIP_VERSION_REG) & 0x0f; /* Only bits 3-0 */

	if(id != 0xFFFF)
		{
		retval = 1;
		if(output_type == VERBOSE_DISPLAY || output_type == DEBUG_DISPLAY)
			printf("Found an ITE IT8718F (id 0x%04x, version 0x%02x) at special address port 0x%02x\n\n", id, chipver, port);

		spec_addr_port = port;

		/* Get EC base address */
		regwrite(port, LDN_SELECT_REG, EC_LDN);
		ec_port = regread(port, EC_BASE_ADDR_MSB) << 8;
		ec_port |= regread(port, EC_BASE_ADDR_LSB);

		if(debug)
			fprintf(stderr, "DEBUG: Environment Controller base address=0x%04x\n", ec_port);

		/* EC address register = EC base address + 5 */
		ec_port += 5;
	
		if(debug)
			fprintf(stderr, "DEBUG: Environment Controller port=0x%04x\n", ec_port);

		}
	else
		{
		if(debug)
			fprintf(stderr, "DEBUG: Did not find an ITE IT8718F at special address port 0x%x\n", port);
		}
	       
	exit_config_mode(port);

	return(retval);
}


/**********************
 ** open_lockfile()  **
 **********************/

void open_lockfile(void)
{
	lockfile_fd=open(LOCK_FILE,O_RDWR|O_CREAT|O_EXLOCK|O_NONBLOCK,0640);
			
	if (lockfile_fd < 0) 
		{
		perror(LOCK_FILE);
		fprintf(stderr, "Is it8718fd already running?\n");
		exit(EXIT_FAILURE); 
		}
}


/******************
 ** write_pid()  **
 ******************/

void write_pid(void)
{
	char tmpstr[sizeof(pid_t)] = "";

	snprintf(tmpstr, sizeof(tmpstr), "%i", getpid());
	if(write(lockfile_fd, tmpstr, strlen(tmpstr)) < 0)
		{
		perror("Cannot write to pid file");
		exit(EXIT_FAILURE);
		}
}


/*******************
 **  dump_data()  **
 *******************/

void dump_data(uint16_t port, uint8_t ldn_sel, int output_type)
{
	char out_buffer[BUFFER_LEN] = "";

	regwrite(port, LDN_SELECT_REG, ldn_sel); 

	/* verbose display */
	if(ldn_sel == EC_LDN && output_type == VERBOSE_DISPLAY)
		{
		printf("%s %dC\n", tempin1_label, regread(port, 0x29));
		printf("%s %dC\n", tempin2_label, regread(port, 0x2A));
		printf("%s %dC\n", tempin3_label, regread(port, 0x2B));
	
		/* only notify if readings unbreliable */
		(regread(port, 0x0C) & 0x07) ? printf("\n") : printf("\n16 bit fan counters inactive; rpm readings unreliable\n");

		printf("%s %d rpm\n", fan1_label, CALC_FAN_RPM((regread(port, 0x0D) | regread(port, 0x18) << 8)));
		printf("%s %d rpm\n", fan2_label, CALC_FAN_RPM((regread(port, 0x0E) | regread(port, 0x19) << 8)));
		printf("%s %d rpm\n", fan3_label, CALC_FAN_RPM((regread(port, 0x0F) | regread(port, 0x1A) << 8)));
		
		printf("\n");

		printf("%s\t %0.2f\n", vin0_label, regread(port, 0x20) * 0.016);
		printf("%s\t %0.2f\n", vin1_label, regread(port, 0x21) * 0.016);
		printf("%s\t %0.2f\n", vin2_label, regread(port, 0x22) * 0.016);
		printf("%s\t %0.2f\n", vin3_label, regread(port, 0x23) * 1.68 * 0.016);
		printf("%s\t %0.2f\n", vin4_label, regread(port, 0x24) * 3.8  * 0.016);
		printf("%s\t -%0.2f\n", vin5_label, regread(port, 0x25) * 3.8 * 0.016);
		printf("%s\t -%0.2f\n", vin6_label, regread(port, 0x26) * 1.68 * 0.016);
		printf("%s\t %0.2f\n", vin7_label, regread(port, 0x27) * 1.68 * 0.016);
		printf("%s\t %0.2f\n", vbat_label, regread(port, 0x28) * 0.016);

		printf("\n");
		}
	/* compact display */
	else if(ldn_sel == EC_LDN && output_type == COMPACT_DISPLAY)
		{
		printf("\n");

		printf("Temp = %d, ", regread(port, 0x29));
		printf("%d, ", regread(port, 0x2A));
		printf("%d\n", regread(port, 0x2B));
		
		printf("RPM = %d, ", CALC_FAN_RPM((regread(port, 0x0D) | regread(port, 0x18) << 8)));
		printf("%d, ", CALC_FAN_RPM((regread(port, 0x0E) | regread(port, 0x19) << 8)));
		printf("%d\n", CALC_FAN_RPM((regread(port, 0x0F) | regread(port, 0x1A) << 8)));
		
		printf("Voltages = %0.2f, ", regread(port, 0x20) * 0.016);
		printf("%0.2f, ", regread(port, 0x21) * 0.016);
		printf("%0.2f, ", regread(port, 0x22) * 0.016);
		printf("%0.2f, ", regread(port, 0x23) * 1.68 * 0.016);
		printf("%0.2f, ", regread(port, 0x24) * 3.8  * 0.016);
		printf("-%0.2f, ", regread(port, 0x25) * 3.8 * 0.016);
		printf("-%0.2f, ", regread(port, 0x26) * 1.68 * 0.016);
		printf("%0.2f, ", regread(port, 0x27) * 1.68 * 0.016);
		printf("%0.2f\n", regread(port, 0x28) * 0.016);
		}
	/* debug display */
	else if(ldn_sel == EC_LDN && output_type == DEBUG_DISPLAY)
		{
		fprintf(stderr, "DEBUG: Dumping port=0x%04x, ldn_sel=0x%02x\n", port, ldn_sel);

		printf("\n");

		printf("TMPIN1 (%s) Register 29h : Value %02xh (%dC)\n", tempin1_label, regread(port, 0x29), regread(port, 0x29));
		printf("TMPIN2 (%s) Register 2ah : Value %02xh (%dC)\n", tempin2_label, regread(port, 0x2A), regread(port, 0x2A));
		printf("TMPIN3 (%s) Register 2bh : Value %02xh (%dC)\n", tempin3_label, regread(port, 0x2B), regread(port, 0x2B));
	
		printf("\n");

		printf("16 bit fan counters (Register 0x0C, Value %02xh) ", regread(port, 0x0C));
		(regread(port, 0x0C) & 0x07) ? printf("active; rpm readings reliable\n") : printf("inactive; rpm readings unreliable\n");

		printf("Fan1 (Fan Tach 1 LSB) Register %02xh : Value %02xh (%d)\n", 0x0D, regread(port, 0x0D), regread(port, 0x0D));
		printf("Fan1 (Fan Tach 1 MSB) Register %02xh : Value %02xh (%d)\n", 0x18, regread(port, 0x18), regread(port, 0x18));
		printf("Fan1 (%s) %d rpm\n", fan1_label, CALC_FAN_RPM((regread(port, 0x0D) | regread(port, 0x18) << 8)));

		printf("Fan2 (Fan Tach 2 LSB) Register %02xh : Value %02xh (%d)\n", 0x0E, regread(port, 0x0E), regread(port, 0x0E));
		printf("Fan2 (Fan Tach 2 MSB) Register %02xh : Value %02xh (%d)\n", 0x19, regread(port, 0x19), regread(port, 0x19));
		printf("Fan2 (%s) %d rpm\n", fan2_label, CALC_FAN_RPM((regread(port, 0x0E) | regread(port, 0x19) << 8)));

		printf("Fan3 (Fan Tach 3 LSB) Register %02xh : Value %02xh (%d)\n", 0x0F, regread(port, 0x0F), regread(port, 0x0F));
		printf("Fan3 (Fan Tach 3 MSB) Register %02xh : Value %02xh (%d)\n", 0x1A, regread(port, 0x1A), regread(port, 0x1A));
		printf("Fan3 (%s) %d rpm\n", fan3_label, CALC_FAN_RPM((regread(port, 0x0F) | regread(port, 0x1A) << 8)));
		
		printf("\n");

		printf("VIN0 (%s) Register %02xh : Value %02xh (%0.2f)\n", vin0_label, 0x20, regread(port, 0x20), regread(port, 0x20) * 0.016);
		printf("VIN1 (%s) Register %02xh : Value %02xh (%0.2f)\n", vin1_label, 0x21, regread(port, 0x21), regread(port, 0x21) * 0.016);
		printf("VIN2 (%s) Register %02xh : Value %02xh (%0.2f)\n", vin2_label, 0x22, regread(port, 0x22), regread(port, 0x22) * 0.016);
		printf("VIN3 (%s) Register %02xh : Value %02xh (%0.2f)\n", vin3_label, 0x23, regread(port, 0x23), regread(port, 0x23) * 1.68 * 0.016);
		printf("VIN4 (%s) Register %02xh : Value %02xh (%0.2f)\n", vin4_label, 0x24, regread(port, 0x24), regread(port, 0x24) * 3.8  * 0.016);
		printf("VIN5 (%s) Register %02xh : Value %02xh (-%0.2f)\n", vin5_label, 0x25, regread(port, 0x25), regread(port, 0x25) * 3.8 * 0.016);
		printf("VIN6 (%s) Register %02xh : Value %02xh (-%0.2f)\n", vin6_label, 0x26, regread(port, 0x26), regread(port, 0x26) * 1.68 * 0.016);
		printf("VIN7 (%s) Register %02xh : Value %02xh (%0.2f)\n", vin7_label, 0x27, regread(port, 0x27), regread(port, 0x27) * 1.68 * 0.016);
		printf("VBAT (%s) Register %02xh : Value %02xh (%0.2f)\n", vbat_label, 0x28, regread(port, 0x28), regread(port, 0x28) * 0.016);
		}
	/* syslog display */
	else if(ldn_sel == EC_LDN && output_type == SYSLOG_DISPLAY)
		{
		snprintf(out_buffer, sizeof(out_buffer), "Temp %d, %d, %d; RPM %d, %d, %d; Volts %0.2f, %0.2f, %0.2f, %0.2f, %0.2f, -%0.2f, -%0.2f, %0.2f, %0.2f",\
			regread(ec_port, 0x29),\
			regread(ec_port, 0x2A),\
			regread(ec_port, 0x2B),\
			CALC_FAN_RPM((regread(ec_port, 0x0D) | regread(ec_port, 0x18) << 8)),\
			CALC_FAN_RPM((regread(ec_port, 0x0E) | regread(ec_port, 0x19) << 8)),\
			CALC_FAN_RPM((regread(ec_port, 0x0F) | regread(ec_port, 0x1A) << 8)),\
			regread(ec_port, 0x20) * 0.016,\
			regread(ec_port, 0x21) * 0.016,\
			regread(ec_port, 0x22) * 0.016,\
			regread(ec_port, 0x23) * 1.68 * 0.016,\
			regread(ec_port, 0x24) * 3.8  * 0.016,\
			regread(ec_port, 0x25) * 3.8 * 0.016,\
			regread(ec_port, 0x26) * 1.68 * 0.016,\
			regread(ec_port, 0x27) * 1.68 * 0.016,\
			regread(ec_port, 0x28) * 0.016);

		syslog(SYSLOG_LEVEL, out_buffer);
		}
}


/**********************
 **  daemon_reply()  **
 **********************/

int daemon_reply(char *buffer, int ldn_sel)
{
	regwrite(spec_addr_port, LDN_SELECT_REG, ldn_sel); 

	sprintf(buffer, "Temp %d, %d, %d\nRPM %d, %d, %d\nVcore %0.2f\nVDDR %0.2f\nVoltages %0.2f, %0.2f, %0.2f, -%0.2f, -%0.2f\nVSB %0.2f\nVbat %0.2f\n",\
		regread(ec_port, 0x29),\
		regread(ec_port, 0x2A),\
		regread(ec_port, 0x2B),\
		CALC_FAN_RPM((regread(ec_port, 0x0D) | regread(ec_port, 0x18) << 8)),\
		CALC_FAN_RPM((regread(ec_port, 0x0E) | regread(ec_port, 0x19) << 8)),\
		CALC_FAN_RPM((regread(ec_port, 0x0F) | regread(ec_port, 0x1A) << 8)),\
		regread(ec_port, 0x20) * 0.016,\
		regread(ec_port, 0x21) * 0.016,\
		regread(ec_port, 0x22) * 0.016,\
		regread(ec_port, 0x23) * 1.68 * 0.016,\
		regread(ec_port, 0x24) * 3.8  * 0.016,\
		regread(ec_port, 0x25) * 3.8 * 0.016,\
		regread(ec_port, 0x26) * 1.68 * 0.016, \
		regread(ec_port, 0x27) * 1.68 * 0.016,\
		regread(ec_port, 0x28) * 0.016);
	
	return(0);
}


/************************
 **  signal_handler()  **
 ************************/

void signal_handler(int sig)
{
	switch(sig) 
		{
		case SIGHUP:
			syslog(SYSLOG_LEVEL,"Hangup signal caught, re-reading %s", config_file);
			read_config(config_file);
			populate_labels();
			break;
		case SIGTERM:
			syslog(SYSLOG_LEVEL,"Terminate signal caught; cleaning up and exiting");
			close(lockfile_fd);
			unlink(LOCK_FILE);
			exit(EXIT_SUCCESS);
			break;
		case SIGALRM:
			dump_data(ec_port, EC_LDN, SYSLOG_DISPLAY);
			break;
		}
}
