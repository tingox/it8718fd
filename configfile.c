/***************************************************************************
 **
 **  $Id: configfile.c,v 1.3 2009/01/05 06:39:42 trev Exp $
 **
 ***************************************************************************
 **
 **  COPYRIGHT NOTICE
 ** 
 **  Copyright 2009 Trevor B Roydhouse (trev (at) sentry (dot) org)
 **  All rights reserved
 **  
 **  Redistribution and use in source and binary forms, with or without
 **  modification, is permitted provided that the following conditions
 **  are met:
 **
 **  1. Redistributions of source code must retain the above copyright
 **     notice, this list of conditions and the following disclaimer.
 **
 **  2. Redistributions in binary form must reproduce the above copyright
 **     notice, this list of conditions and the following disclaimer in the
 **     documentation and/or other materials provided with the distribution.
 **
 **  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND ANY CONTRIBUTORS "AS IS" 
 **  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 **  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 **  PURPOSE ARE HEREBY DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR ANY 
 **  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 **  EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 **  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 **  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY 
 **  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 **  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 **  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **
 ***************************************************************************
 **
 **  Name    : it8718fd
 **  File    : configfile.c
 **  Date    : 3 January 2009
 **  Author  : Trevor B Roydhouse, BJuris, LLB, LLM (UNSW)
 **
 **  Purpose : Handle configuration file reading, parsing.
 **
 ***************************************************************************/


/* Includes */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include "configfile.h"


/* Defines - see configfile.h */


/* Prototypes - see configfile.h */


/* Globals */

char linebuffer[CONF_PARM_MAX][CONF_LINE_MAX];

char tempin1_label[LABEL_LEN_MAX];
char tempin2_label[LABEL_LEN_MAX];
char tempin3_label[LABEL_LEN_MAX];

char fan1_label[LABEL_LEN_MAX];
char fan2_label[LABEL_LEN_MAX];
char fan3_label[LABEL_LEN_MAX];

char vin0_label[LABEL_LEN_MAX];
char vin1_label[LABEL_LEN_MAX];
char vin2_label[LABEL_LEN_MAX];
char vin3_label[LABEL_LEN_MAX];
char vin4_label[LABEL_LEN_MAX];
char vin5_label[LABEL_LEN_MAX];
char vin6_label[LABEL_LEN_MAX];
char vin7_label[LABEL_LEN_MAX];
char vbat_label[LABEL_LEN_MAX];


/************
 **  MAIN  **
 ************/

#ifdef STANDALONE
int main(int argc, char *argv[])
{
	populate_label_defaults();

	if( read_config("it8718fd.conf") != 1)
		{
		exit(EXIT_FAILURE);
		}

	populate_labels();

	/* if(debug) */
		dump_labels();
	
	exit(EXIT_SUCCESS);
}
#endif

int read_config(char *filename)
{
int retval = 1;
FILE *cfp = NULL;
int lineno = 1;
char tmpbuffer[BUFFER_LEN] = "";

	/* open config file */
	if ((cfp = fopen(filename, "r")) == NULL)
		{
		if(errno == ENOENT)
			return(0);

		sprintf(tmpbuffer, "Unable to open %s", filename);
		perror(tmpbuffer);
		return(0);
		}
	else
		{
		if(debug)
			fprintf(stderr, "DEBUG: %s opened\n", filename);
		}

	/* filesize sanity check */
	if (fseek (cfp, 0L, SEEK_END) != 0)
		{
		perror(filename);
		fprintf(stderr, "Unable to fseek %s, using defaults\n", filename);
		return(0);
		}
	else
		{
		if(debug)
			fprintf(stderr, "DEBUG: %s fseeked\n", filename);
		}

	if (ftell(cfp) < MIN_CONFIG_FILE_SIZE || ftell(cfp) > MAX_CONFIG_FILE_SIZE)
		{
		fprintf(stderr, "Unreasonable file size of %li bytes for %s,  using defaults\n", ftell(cfp), filename);
		return(0);
		}
	else
		{
		if(debug)
			fprintf(stderr, "DEBUG: Configuration file size %li bytes\n", ftell(cfp));
		
		rewind (cfp);
		}

	/* read config file lines */
	 while (fgets(linebuffer[lineno], CONF_LINE_MAX, cfp) != NULL)
		{
		/* trim any leading spaces */
		while(linebuffer[lineno][0] == ' ')
			{
			strcpy(linebuffer[lineno], linebuffer[lineno] + 1);

			if(debug)
				fprintf(stderr, "DEBUG: Trimming leading spaces: %s", linebuffer[lineno]);
			}

		/* ignore blank lines */
		if(strlen(linebuffer[lineno]) < 2)
			{
			if(debug)
				fprintf(stderr, "DEBUG: Ignoring blank line\n");

			continue;
			}

		/* ignore comment lines */
		if(linebuffer[lineno][0] == '#')
			{
			if(debug)
				fprintf(stderr, "DEBUG: Ignoring comment line: %s", linebuffer[lineno]);

			continue;
			}

		/* truncate lines with a trailing comment */
		if(str_index(linebuffer[lineno], "#") != -1)
			{
			if(debug)
				fprintf(stderr, "DEBUG: Truncating line with trailing comment: %s", linebuffer[lineno]);

			linebuffer[lineno][str_index(linebuffer[lineno], "#") + 1] = '\0';
			linebuffer[lineno][str_index(linebuffer[lineno], "#")] = '\n';
			}

		/* ignore excessively long lines */
		if(linebuffer[lineno][strlen(linebuffer[lineno]) - 1] != '\n')
			{
			fprintf(stderr, "Ignoring %s line longer than %i characters: %s\n", filename, CONF_LINE_MAX, linebuffer[lineno]);

			/* now make sure we find the end of the long line before continuing */
			while(fgetc(cfp) != '\n')
					continue;

			continue;
			}

		/* ignore lines with no equals sign */
		if(str_index(linebuffer[lineno], "=") == -1)
			{
			fprintf(stderr, "Ignoring %s line without equals sign: %s", filename, linebuffer[lineno]);

			continue;
			}
		

		if(debug)
			fprintf(stderr, "DEBUG: [Line %i] %s", lineno, linebuffer[lineno]);
			
		lineno++;
		continue;
		}

return(retval);	
}


void populate_label_defaults(void)
{
strncpy(tempin1_label, TEMPIN1_LABEL, LABEL_LEN_MAX);
strncpy(tempin2_label, TEMPIN2_LABEL, LABEL_LEN_MAX);
strncpy(tempin3_label, TEMPIN3_LABEL, LABEL_LEN_MAX);

strncpy(fan1_label, FAN1_LABEL, LABEL_LEN_MAX);
strncpy(fan2_label, FAN2_LABEL, LABEL_LEN_MAX);
strncpy(fan3_label, FAN3_LABEL, LABEL_LEN_MAX);

strncpy(vin0_label, VIN0_LABEL, LABEL_LEN_MAX);
strncpy(vin1_label, VIN1_LABEL, LABEL_LEN_MAX);
strncpy(vin2_label, VIN2_LABEL, LABEL_LEN_MAX);
strncpy(vin3_label, VIN3_LABEL, LABEL_LEN_MAX);
strncpy(vin4_label, VIN4_LABEL, LABEL_LEN_MAX);
strncpy(vin5_label, VIN5_LABEL, LABEL_LEN_MAX);
strncpy(vin6_label, VIN6_LABEL, LABEL_LEN_MAX);
strncpy(vin7_label, VIN7_LABEL, LABEL_LEN_MAX);
strncpy(vbat_label, VBAT_LABEL, LABEL_LEN_MAX);
}


void populate_labels(void)
{
int count = 0;
char parameter[CONF_LINE_MAX];
char value[CONF_LINE_MAX];

	for(count = 1; count < CONF_PARM_MAX; count++)
		{
		sscanf(linebuffer[count], "%[^=]=%[-+.A-Za-z0-9 ]", parameter, value);

		/* strip any leading spaces for value */
		while(value[0] == ' ')
			strcpy(value, value + 1);

		/* strip any trailing spaces for value */
		while(value[strlen(value) - 1] == ' ')
			value[strlen(value) - 1] = '\0';

		/* check for new parameter values */
		if(str_index(parameter, "tempin1") != -1)
			strncpy(tempin1_label, value, LABEL_LEN_MAX);
		else if(str_index(parameter, "tempin2") != -1)
			strncpy(tempin2_label, value, LABEL_LEN_MAX);
		else if(str_index(parameter, "tempin3") != -1)
			strncpy(tempin3_label, value, LABEL_LEN_MAX);
		else if(str_index(parameter, "fan1") != -1)
			strncpy(fan1_label, value, LABEL_LEN_MAX);
		else if(str_index(parameter, "fan2") != -1)
			strncpy(fan2_label, value, LABEL_LEN_MAX);
		else if(str_index(parameter, "fan3") != -1)
			strncpy(fan3_label, value, LABEL_LEN_MAX);
		else if(str_index(parameter, "vin0") != -1)
			strncpy(vin0_label, value, LABEL_LEN_MAX);
		else if(str_index(parameter, "vin1") != -1)
			strncpy(vin1_label, value, LABEL_LEN_MAX);
		else if(str_index(parameter, "vin2") != -1)
			strncpy(vin2_label, value, LABEL_LEN_MAX);
		else if(str_index(parameter, "vin3") != -1)
			strncpy(vin3_label, value, LABEL_LEN_MAX);
		else if(str_index(parameter, "vin4") != -1)
			strncpy(vin4_label, value, LABEL_LEN_MAX);
		else if(str_index(parameter, "vin5") != -1)
			strncpy(vin5_label, value, LABEL_LEN_MAX);
		else if(str_index(parameter, "vin6") != -1)
			strncpy(vin6_label, value, LABEL_LEN_MAX);
		else if(str_index(parameter, "vin7") != -1)
			strncpy(vin7_label, value, LABEL_LEN_MAX);
		else if(str_index(parameter, "vbat") != -1)
			strncpy(vbat_label, value, LABEL_LEN_MAX);
		else
			{
			fprintf(stderr, "Unknown parameter: '%s'\n", parameter);
			continue;
			}

		if(debug)
			fprintf(stderr, "DEBUG: parameter (%s) : value (%s)\n", parameter, value);
		}

}


void dump_labels(void)
{
printf("TEMPIN1_LABEL %s\n", tempin1_label);
printf("TEMPIN2_LABEL %s\n", tempin2_label);
printf("TEMPIN3_LABEL %s\n", tempin3_label);

printf("FAN1_LABEL %s\n", fan1_label);
printf("FAN2_LABEL %s\n", fan2_label);
printf("FAN3_LABEL %s\n", fan3_label);

printf("VIN0_LABEL %s\n", vin0_label);
printf("VIN1_LABEL %s\n", vin1_label);
printf("VIN2_LABEL %s\n", vin2_label);
printf("VIN3_LABEL %s\n", vin3_label);
printf("VIN4_LABEL %s\n", vin4_label);
printf("VIN5_LABEL %s\n", vin5_label);
printf("VIN6_LABEL %s\n", vin6_label);
printf("VIN7_LABEL %s\n", vin7_label);
printf("VBAT_LABEL %s\n", vin0_label);
}


/*****************
 *  str_index()  *
 *****************/
   
int str_index(char string[], char substring[])
{ 
register int i, j, k;
    
for(i = 0; string[i] != '\0'; i++)
	for(j = i, k= 0; substring[k] == string[j]; k++, j++)
		if(substring[k+1] == '\0')
			return(i);  /* substring found at i */

return(-1);     /* substring was not found */
}
